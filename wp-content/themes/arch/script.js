jQuery(document).ready(function($) {
	
	
	jQuery('ul.nav li.dropdown').click(function(e) {
		if ($(window).width() > 767) {
			window.location = $(this).find('a').attr('href');
			e.preventDefault();
		}
	});
	
	
	
	
	jQuery('#sidebar').on('click', '#notsure .clickapply a', function(e) {
		_gaq.push(['_trackEvent', 'JobsBoard', 'Unsure', 'JobsBoard']);
		console.log("GA Event - Unsure button clicked");
	});	
	
	
	
	
	// feedback button is dynamically positioned so we have to do this
	$live_chat = jQuery('#livechat, #live-chat').filter(':first');
	$live_chat.css({ top : (jQuery(window).height() / 2) - (43.5 + 5) -($live_chat.outerHeight()) + 'px' });
	
	
	
	more_brands_text = $('.more-brands').text();
	
	$('.more-brands').click(function(e) {
		
		var $this = $(this);
		$hidden_brands = $('.hidden-brands');
		
		e.preventDefault();
		$hidden_brands.slideToggle(function() {
			$this.text(function(i) {
				//debugger;
				return (!$hidden_brands.is(':visible') ?  more_brands_text : '- Show less Brands');
			});
		});
		
	});
	
	
});