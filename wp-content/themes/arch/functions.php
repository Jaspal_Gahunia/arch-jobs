<?php



require_once(get_stylesheet_directory() . '/includes/widgets.php');

add_action('init', function(){
	remove_action('job_footer', 'jr_application_form', 1);
});

add_image_size( 'thumb', 100, 50);

   


add_action('wp_enqueue_scripts', 'main_site_assets');
function main_site_assets() {
	wp_enqueue_style('main-site-css', MAIN_SITE_URL . 'wp-content/themes/arch2016/css/structure.css', array('at-main-css'));
	
	//wp_enqueue_style('bootstrap-css', '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css');
	wp_enqueue_script('jquery');
	wp_enqueue_script('boostrap-js', '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js');
	
	wp_enqueue_script('main-script', get_stylesheet_directory_uri(). '/script.js');
	
	wp_enqueue_script('structure-js', MAIN_SITE_URL . 'wp-content/themes/arch2016/js/structure.js', array('jquery'));
	
	
	wp_enqueue_style('comfortaa-font', 'https://fonts.googleapis.com/css?family=Comfortaa:400,700,300');
	wp_enqueue_style('robotto-font', 'https://fonts.googleapis.com/css?family=Roboto:400,100,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic');
	wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
	
	wp_enqueue_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js');
	
}

   
function get_excerpt($count){
	$permalink = get_permalink($post->ID);
	$excerpt = get_the_content();
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $count);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = $excerpt.'... <div class="clear"></div><a href="' . $permalink . '">Read More / Apply For This Job Now <span class="fa fa-angle-right hvr-icon-wobble-horizontal" aria-hidden="true"></span></a>';
	return $excerpt;
}



function add_job_meta_box_slug() {

	add_meta_box(
		'job-listing-slug',
		'Job name for dropdown',
		'job_meta_box_slug',
		'job_listing',
		'side'
	);

}

add_action('add_meta_boxes', 'add_job_meta_box_slug');
add_action('save_post', 'save_job_slug');

function job_meta_box_slug() {
	global $post;

	
	$job_slug = get_post_meta($post->ID, 'job_slug_dd', true);
	
	/*
	if (!$job_slug) {
		$job_company = get_post_meta($post->ID, '_Company', true);
		$job_slug = ($post->post_title . ($job_company ? ' - ' . $job_company : ''));
	}
	*/
	
	echo '<p style="font-size:13px; font-style:italic; color:#666; margin:6px 0;">Enter value to go in dropdown here:</p><input type="text" name="job_slug_dd" value="' . esc_attr($job_slug) . '" style="width:220px;" />';

}

function save_job_slug($id) {
	global $post;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $id;
	}

	if (!current_user_can('edit_page', $id)) {
		return $id;
	}

	
	/*
	$job_company = get_post_meta($post->ID, '_Company', true);
	$job_slug_custom = ($_POST['job_slug_dd'] ? $_POST['job_slug_dd'] : ($post->post_title . ($job_company ? ' - ' . $job_company : '')));
	*/
	
	
	//add_post_meta($post->ID, 'job_slug_dd', $job_slug_custom);
	
	
	update_post_meta($id, 'job_slug_dd', $_POST['job_slug_dd']);


}



function get_meta_values( $key = '', $type = 'post', $status = 'publish' ) {

	global $wpdb;

	if( empty( $key ) )
		return;

	$r = $wpdb->get_col( $wpdb->prepare( "
			SELECT distinct pm.meta_value FROM {$wpdb->postmeta} pm
			LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
			WHERE pm.meta_key = '%s'
			AND p.post_status = '%s'
			AND p.post_type = '%s'
			", $key, $status, $type ) );

	return $r;
}


add_action('pre_get_posts', 'add_companies_search');
function add_companies_search($query) {
	if (!is_admin() && $query->is_main_query() && $_GET['ptype'] == 'job_listing') {

		$query->set('meta_query', array(
			array(
				'key' => '_Company',
				'value' => $_GET['company'],
				'compare' => 'LIKE',
			)	
		));
	}
}


add_filter( 'template_include', 'portfolio_page_template', 99 );
function portfolio_page_template( $template ) {
	if (!is_admin() && $_GET['ptype'] == 'job_listing' && empty($_GET['job_type']) && empty($_GET['job_cat']) && !is_tax()) {

		query_posts(array(
				'post_type'   => 'job_listing',
				'post_status' => 'publish',
				'meta_query'  => array(
					array(
						'key' => '_Company',
						'value' => $_GET['company'],
						'compare' => 'LIKE',
					)
				)	
		));
		
		$template = locate_template( array( 'taxonomy-job.php' ) );
	}

	return $template;
}



add_action('wp_footer', 'add_engage_tag');
function add_engage_tag() {
	
	/* so we can get livechat on jobs board */
	
	?>
	
	
	<script type="text/javascript"><!--//<![CDATA[
	   if(location.protocol=="https:")
	   {
	   var dplat = "https://bobsguide.logo-net.co.uk/Delivery/DBURLssl.php";
	   }
	   else
	   {
	   var dplat = "http://bobsguide.logo-net.co.uk/Delivery/DBURL.php";
	   }
	   var strPURL = parent.document.URL;
	   strPURL = strPURL.replace(/&/g, "^");
	   strPURL = strPURL.toLowerCase();
	   strPURL = strPURL.replace(/</g, "-1");
	   strPURL = strPURL.replace(/>/g, "-2");
	   strPURL = strPURL.replace(/%3c/g, "-1");
	   strPURL = strPURL.replace(/%3e/g, "-2");
	   var T = new Date();
	   var cMS = T.getTime();
	   document.write ("<scr"+"ipt type='text/javascript' src='"+dplat);
	   document.write ("?SDTID=171");
	   document.write ('&PURL=' + strPURL);
	   document.write ('&CMS=' + cMS);
	   document.write ("'></scr"+"ipt>");
	//]]>--></script>
	
	<?php 
	
	

	
	
}




add_action('wp_head', 'add_google_site_ver_tag');
function add_google_site_ver_tag() {

	?>

	<meta name="google-site-verification" content="qATEwm2YAZ935d7c-AORbt0yXeDvu5wDmoQn_yQbE4M" />

	<?php 
	
}



function arch_get_external_html($url, $key)
{
	$html = get_transient($key);
	if ($html === false || isset($_GET['refresh_external_html'])) {
		$html =	file_get_contents($url);
		set_transient($key, $html, 12 * HOUR_IN_SECONDS);
	} 

	return $html;
}











