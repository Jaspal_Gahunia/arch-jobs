<div id="sidebar">

<div id="notsure">Not sure which apprenticeship to apply for?
  <div class="clickapply">
  <a href="http://www.archapprentices.co.uk/application-form/?role=match-role">Click Here <span class="fa fa-angle-right hvr-icon-wobble-horizontal" aria-hidden="true"></span></a>
  </div>
</div>

	<ul class="widgets">
	
		<?php appthemes_before_sidebar_widgets(); ?>
		
		<?php get_template_part( 'includes/sidebar-nav' ); ?>

		<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar_main')) : else : ?>

			<!-- no dynamic sidebar setup -->

		<?php endif; ?>
		
		<?php appthemes_after_sidebar_widgets(); ?>

	</ul>

</div><!-- end sidebar -->