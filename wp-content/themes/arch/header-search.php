<?php global $app_abbr, $header_search; $header_search = true; $wp_query; ?>



<?php if (get_option('jr_show_searchbar')!=='no' && ( !isset($_GET['submit']) || ( isset($_GET['submit']) && $_GET['submit']!=='true' ) ) && ( !isset($_GET['myjobs']) || ( isset($_GET['myjobs']) && $_GET['myjobs']!=='true' ) ) ) : ?>

	<form action="<?php echo esc_url( home_url() ); ?>/" method="get" id="searchform">

		<div class="search-wrap">

			<div style="width: 100%;">
			
			<!-- 
                <select id="search" name="s" class="search"style=" background: transparent; border: none;">
					<option value=""  selected='selected'>All Jobs</option>
					<option value="Business Admin">Business Admin</option>
					<option value="Business Analyst">Business Analyst</option>
					<option value="Creative & Digital Media">Creative & Digital Media</option>
					<option value="Customer Services">Customer Services</option>
					<option value="Digital Marketing">Digital Marketing</option>
					<option value="Financial Services">Financial Services</option>
					<option value="IT Technician">IT</option>
					<option value="Social Media">Social Media</option>
					<option value="Web Development">Web Development</option>
				</select>
			-->

			
			<select name="job_type" class="search" >
				<option value="">All Jobs</option>
				<?php 
				
				$job_types = get_terms('job_type'); 
				
				foreach($job_types as $job_type): ?>
					<option value="<?php echo $job_type->slug; ?>"<?php echo ($job_type->slug == get_query_var('job_type') ? ' selected="selected"' : ''); ?>><?php echo $job_type->name; ?></option>
				<?php endforeach; ?>
				
			</select>
			
		<?php $job_locations = get_terms('job_cat'); ?>
		<select name="job_cat" id="near" class="search">
			<option value="">All Locations</option>
			<?php foreach($job_locations as $job_location): ?>
				<option value="<?php echo $job_location->slug; ?>"<?php echo ($job_location->slug == get_query_var('job_cat') ? ' selected="selected"' : ''); ?>><?php echo $job_location->name; ?></option>
			<?php endforeach; ?>
		</select>			
			
			
			<input type="text" class="text" name="company" value="<?php echo (!empty($_GET['company']) ? esc_attr($_GET['company']) : ''); ?>" placeholder="Company (optional)" />
			
			
			
				<!-- <input type="text" id="near" title="<?php _e('Location',APP_TD); ?>" name="location" class="text" placeholder="<?php _e('Location',APP_TD); ?>" value="<?php if (isset($_GET['location'])) echo esc_attr($_GET['location']); ?>" /> -->
				<label for="search"><button type="submit" title="<?php _e('Go',APP_TD); ?>" class="submit"><?php _e('Go',APP_TD); ?></button></label>

				<input type="hidden" name="ptype" value="<?php echo esc_attr( APP_POST_TYPE ); ?>" />
				

				<input type="hidden" name="latitude" id="field_latitude" value="" />
				<input type="hidden" name="longitude" id="field_longitude" value="" />
				<input type="hidden" name="full_address" id="field_full_address" value="" />
				<input type="hidden" name="north_east_lng" id="field_north_east_lng" value="" />
				<input type="hidden" name="south_west_lng" id="field_south_west_lng" value="" />
				<input type="hidden" name="north_east_lat" id="field_north_east_lat" value="" />
				<input type="hidden" name="south_west_lat" id="field_south_west_lat" value="" />
			</div>

		<?php echo ''; //jr_radius_dropdown(); ?>
		
		
		<!-- 
			<select name="_Company">
			<option value="">All Companies</option>
			<?php foreach(get_meta_values('_Company', 'job_listing') as $company): ?>
				<option value="<?php echo $company; ?>"><?php echo $company; ?></option>
			<?php endforeach; ?>
		</select>
		-->
		
		
		

		
		
		
		<!-- 
		<div class="radius">
			<label for="radius">Radius:</label>
			<select name="radius" class="radius">
				<?php foreach (array(1, 5, 10, 50, 100) as $rad): ?>
					<option value="<?php echo $rad; ?>"<?php echo ((!get_query_var('radius') && $rad == 50) || $rad == get_query_var('radius') ? ' selected="selected"' : ''); ?>><?php echo $rad . ' mi'; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		-->

		</div><!-- end search-wrap -->

	</form>

<?php endif; ?>


<div class="role-not-sure">
	<p>Not sure which apprenticeship to apply for? <a href="http://www.archapprentices.co.uk/application-form/?role=match-role">Click here.</a></p>
</div>
