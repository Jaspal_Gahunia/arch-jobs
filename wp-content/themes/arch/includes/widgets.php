<?php




add_action( 'widgets_init', 'arch_board_load_widgets' );
// register and load all custom widgets
function arch_board_load_widgets() {

	register_widget( 'TopApprenticeships' );

}




// Creating the widget
class TopApprenticeships extends WP_Widget {

	function __construct() {
		parent::__construct(
				// Base ID of your widget
				'top_apprenticeships',

				// Widget name will appear in UI
				__('Top Apprenticeships', 'lbbd'),

				// Widget description
				array( 'description' => __( 'Show Branded Apprenticeships from the main Arch site.', 'lbbd' ), )
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		ob_start();

		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes

		echo $args['before_widget'];



		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output

		
        $types = get_transient('top_apprenticeships');
        
        if ( $types === false) {
		
		    $types = wp_remote_get('http://www.archapprentices.co.uk/page_cat/brand/?json=get_posts&posts_per_page=50', array('timeout'=>15));
            if (!is_wp_error($types))            
                set_transient('top_apprenticeships', $types, 6 * HOUR_IN_SECONDS); 
        }
		
		if (!is_wp_error($types)): 
		
			$types = json_decode($types['body']);
			
			//var_dump($types);
			
			
			
			?>
			
			<ul class="top-apprenticeships no-list">
				<?php foreach (array_slice($types->posts, 0, 10) as $top_app): ?>
					<li><a href="<?php echo esc_url($top_app->url); ?>"><?php echo esc_html($top_app->title); ?></a></li>
				<?php endforeach; ?>
			</ul>
			<a href="#" class="more-brands">+ Show more brands</a>
			<ul class="hidden-brands no-list">
				<?php foreach (array_slice($types->posts, 10, 40) as $top_app): ?>
					<li><a href="<?php echo esc_url($top_app->url); ?>"><?php echo esc_html($top_app->title); ?></a></li>
				<?php endforeach; ?>
			</ul>
			
							
			<?php 
			
		endif;




		echo $args['after_widget'];


	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'lbbd' );
		}



		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>


		
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		
		return $instance;
	}
	
	
} 







