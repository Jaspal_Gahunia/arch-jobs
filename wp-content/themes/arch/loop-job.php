<?php
/**
 * Main loop for displaying jobs
 *
 * @package JobRoller
 * @author AppThemes
 *
 */
?>

<?php appthemes_before_loop( 'job_listing' ); ?>


<?php 


global $wp_query;

$posts_found = $wp_query->found_posts;

// if the meta query companies search didnt match anything - then remove it and show the default
if (!empty($_GET['company']) && $posts_found == 0) {
	unset($wp_query->query_vars['meta_query']);
	query_posts($wp_query->query_vars);
}



if (!empty($_GET['company']) && $posts_found == 0):

?>

<div class="noposts-company">
<p>Sorry there were no jobs found with that specific company, however here are some other jobs you may be interested in ..</p>
</div>


<?php endif; ?>

<?php if (have_posts()) : $alt = 1; ?>

    <ol id="job_list" class="jobs">

        <?php while (have_posts()) : the_post(); ?>
		
			<?php appthemes_before_post( 'job_listing' ); ?>

            <?php
				$post_class = array('job');
				$expired = jr_check_expired( $post );

				if ( $expired ) {
					$post_class[] = 'job-expired';
				}
				$alt=$alt*-1;

				if ($alt==1) $post_class[] = 'job-alt';

				if ( !empty($main_wp_query) && jr_is_listing_featured( $post->ID, $main_wp_query ) ) $post_class[] = 'job-featured';
				
				
				if (get_field('remove_job_from_listings', $post->ID)) $post_class[] = 'job-remove';
				
            ?>

            <li class="<?php echo implode(' ', $post_class); ?>">

                <dl>

                    <dd class="type" style="float: right; text-align: center;margin-top: 3px;" ><?php the_post_thumbnail('thumb'); ?></dd>

                    <dt><?php _e('Job', APP_TD); ?></dt>
					
					<?php appthemes_before_post_title( 'job_listing' ); ?>

                    <dd class="title" style="width: 80%;">
						<strong><a class="job_title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
						<?php jr_job_author(); ?><?php $location = get_post_meta($post->ID, 'geo_short_address', true); echo ($location ? ' - ' . esc_html($location) : ''); ?>
                    </dd>

					<?php appthemes_after_post_title( 'job_listing' ); ?>
					
                </dl>
              <div style="margin-top: 10px; padding-top: 5px;">
                <?php echo get_excerpt(225); ?>
                
              </div> 
            </li>
			
			<?php appthemes_after_post( 'job_listing' ); ?>

        <?php endwhile; ?>
		
		<?php appthemes_after_endwhile( 'job_listing' ); ?>

    </ol>

<?php else: ?>

	<?php appthemes_loop_else( 'job_listing' ); ?>        
	
<?php endif; ?>

<?php appthemes_after_loop( 'job_listing' ); ?>