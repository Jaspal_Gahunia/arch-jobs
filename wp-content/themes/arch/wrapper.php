<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if lt IE 7 ]> <html class="ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P86T2C');</script>
	<!-- End Google Tag Manager -->

	
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />

    <title><?php wp_title(''); ?></title>

    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php echo esc_url( $jr_options->jr_feedburner_url ? $jr_options->jr_feedburner_url : get_bloginfo_rss('rss2_url') ); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if ( is_singular() ): wp_enqueue_script( 'comment-reply' ); endif; ?>

    <?php wp_head(); ?>
</head>

<body class="home">

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P86T2C"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php appthemes_before(); ?>

    <div id="wrapper">

		<?php if ( $jr_options->jr_debug_mode ): ?>
			<div class="debug"><h3><?php _e( 'Debug Mode On', APP_TD ); ?></h3><?php print_r( $wp_query->query_vars ); ?></div>
		<?php endif; ?>

		<?php appthemes_before_header(); ?>

		<?php get_header(); ?>

		<?php appthemes_after_header(); ?>

		<div class="clear"></div>
			<div class="main main-page">
				<div class="main-page-inner">
					<div class="fixedwidth">
						<h1 id="maintitle"><span>Apprentice Jobs</span></h1>

				<div id="mainContent" class="<?php global $header_search; if ( ! $header_search ): echo 'nosearch'; endif; ?>">

				<?php if ( $jr_options->breadcrumbs && ! is_front_page() ): ?>

					<div id="breadcrumbs" class="container">
						<div class="row">
							<?php breadcrumb_trail( array(
								'separator'	=> '&raquo;',
								'show_browse' => false,
								'labels' => array(
									'home' => '<div class="breadcrumbs-home"><i class="dashicons-before"></i></div>',
								),
							) ); ?>
						</div>
					</div>

				<?php endif; ?>

				<?php load_template( app_template_path() ); ?>

				<div class="clear"></div>

			</div><!-- end main -->
		</div><!-- end main-page-inner -->
		</div><!-- end fixedwidth -->

		<?php appthemes_before_footer(); ?>

		<?php get_footer( app_template_base() ); ?>

		<?php appthemes_after_footer(); ?>

	</div><!-- end wrapper -->

<?php appthemes_after(); ?>

<?php wp_footer(); ?>

<script type="text/javascript">
_linkedin_data_partner_id = "31044";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>

</body>

</html>
