<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "3dd1b34a26c051beee0aad93403697c01cc1b65562"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/jobroller/header-search.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/jobroller/header-search_2013-11-21-09.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php global $app_abbr, $header_search; $header_search = true; ?>

<?php if (get_option('jr_show_searchbar')!=='no' && ( !isset($_GET['submit']) || ( isset($_GET['submit']) && $_GET['submit']!=='true' ) ) && ( !isset($_GET['myjobs']) || ( isset($_GET['myjobs']) && $_GET['myjobs']!=='true' ) ) ) : ?>

	<form action="<?php echo esc_url( home_url() ); ?>/" method="get" id="searchform">

		<div class="search-wrap">

			<div>
				<input type="text" id="search" title="" name="s" class="text" placeholder="<?php _e('All Jobs',APP_TD); ?>" value="<?php if (isset($_GET['s'])) echo esc_attr(get_search_query()); ?>" />
				<input type="text" id="near" title="<?php _e('Location',APP_TD); ?>" name="location" class="text" placeholder="<?php _e('Location',APP_TD); ?>" value="<?php if (isset($_GET['location'])) echo esc_attr($_GET['location']); ?>" />
				<label for="search"><button type="submit" title="<?php _e('Go',APP_TD); ?>" class="submit"><?php _e('Go',APP_TD); ?></button></label>

				<input type="hidden" name="ptype" value="<?php echo esc_attr( APP_POST_TYPE ); ?>" />

				<input type="hidden" name="latitude" id="field_latitude" value="" />
				<input type="hidden" name="longitude" id="field_longitude" value="" />
				<input type="hidden" name="full_address" id="field_full_address" value="" />
				<input type="hidden" name="north_east_lng" id="field_north_east_lng" value="" />
				<input type="hidden" name="south_west_lng" id="field_south_west_lng" value="" />
				<input type="hidden" name="north_east_lat" id="field_north_east_lat" value="" />
				<input type="hidden" name="south_west_lat" id="field_south_west_lat" value="" />
			</div>

			<?php jr_radius_dropdown(); ?>

		</div><!-- end search-wrap -->

	</form>

<?php endif; ?>
