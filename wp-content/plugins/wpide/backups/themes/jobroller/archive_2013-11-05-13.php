<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "9ae9e8ce90379d13fe0ac8bc0e9e3158691d6344b0"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/jobroller/archive.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/jobroller/archive_2013-11-05-13.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php get_header('search'); ?>

    <div class="section">    

        <?php if (is_category()) { ?>

            <h3 class="pagetitle"><?php single_cat_title(); ?></h3>

        <?php /* If this is a tag archive */
        } elseif( is_tag() ) { ?>

            <h1 class="pagetitle"><?php _e('Posts Tagged',APP_TD); ?> &ldquo;<?php single_tag_title(); ?>&rdquo;</h1>

        <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
            <h1 class="pagetitle"><?php _e('Archive for',APP_TD); ?> <?php the_time('F jS, Y'); ?></h1>

        <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
            <h1 class="pagetitle"><?php _e('Archive for',APP_TD); ?> <?php the_time('F, Y'); ?></h1>

        <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
            <h1 class="pagetitle"><?php _e('Archive for',APP_TD); ?> <?php the_time('Y'); ?></h1>

        <?php /* If this is an author archive */ } elseif (is_author()) { ?>
            <h1 class="pagetitle"><?php _e('Author Archive',APP_TD); ?></h1>

        <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
            <h1 class="pagetitle"><?php _e('Blog Archives',APP_TD); ?></h1>

        <?php } ?>

        <?php get_template_part( 'loop' ); ?>

        <?php jr_paging(); ?>

        <div class="clear"></div>

    </div><!-- end section -->

    <div class="clear"></div>

</div><!-- end main content -->

<?php if (get_option('jr_show_sidebar')!=='no') get_sidebar(); ?>
