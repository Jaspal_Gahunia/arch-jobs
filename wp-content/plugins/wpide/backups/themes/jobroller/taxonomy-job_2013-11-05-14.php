<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "9ae9e8ce90379d13fe0ac8bc0e9e31585c57d27d8e"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/jobroller/taxonomy-job.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/jobroller/taxonomy-job_2013-11-05-14.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
	get_header('search');

	$tax = get_queried_object();

	do_action('jobs_will_display');

	do_action('before_jobs_taxonomy', $tax->taxonomy, $tax->slug);
?>

	<div class="section">

		

		<?php
			$main_wp_query = $wp_query;

			$args = jr_filter_form();
			$args = array_merge(
				array(
					$tax->taxonomy 	=> $tax->slug,
					'post_type' => APP_POST_TYPE,
					'post_status' 	=> 'publish'
				),
				$args
			);
			$args = apply_filters('jr_taxonomy_filter', $args, $tax);

			query_posts($args);
		?>

		<?php appthemes_load_template( 'loop-job.php', array( 'main_wp_query' => $main_wp_query ) ); ?>

		<?php jr_paging(); ?>

		<div class="clear"></div>

	</div><!-- end section -->

	<?php do_action('after_jobs_taxonomy', $tax->taxonomy, $tax->slug); ?>

	<div class="clear"></div>

</div><!-- end main content -->

<?php if (get_option('jr_show_sidebar')!=='no') get_sidebar(); ?>
