<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "ece7d8d9164da7bb5c91eb7852c5984e5c57d27d8e"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/jobroller/includes/sidebar-sjob.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/jobroller/includes/sidebar-sjob_2013-11-06-09.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php if ( JR_Job_Submit_Page::get_id() ) : ?>

	<li class="widget widget-submit">
		
		<?php if (!is_user_logged_in()) : ?>

			<div>
				<a href="<?php echo jr_get_listing_create_url() ?>" class="button"><span><?php _e('Submit a Job',APP_TD); ?></span></a>
				<?php echo jr_get_submit_footer_text(); ?>
			</div>

		<?php endif; ?>

		<?php if (is_user_logged_in() && current_user_can('can_submit_resume')) : ?>

			<?php if (get_option('jr_allow_job_seekers')=='yes') : ?>
				<div>
					<a href="<?php echo get_permalink( JR_Dashboard_Page::get_id() ); ?>" class="button"><span><?php _e('My Dashboard',APP_TD); ?></span></a>
					<?php if ($text = get_option('jr_my_profile_button_text')) echo wpautop(wptexturize($text)); ?>
				</div>
			<?php endif; ?>

		<?php endif; ?>

	</li>

<?php endif; ?>