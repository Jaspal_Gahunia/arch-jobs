<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "3dd1b34a26c051beee0aad93403697c08838ae04c6"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/arch/header-search.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/arch/header-search_2014-02-18-16.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php global $app_abbr, $header_search; $header_search = true; ?>

<?php if (get_option('jr_show_searchbar')!=='no' && ( !isset($_GET['submit']) || ( isset($_GET['submit']) && $_GET['submit']!=='true' ) ) && ( !isset($_GET['myjobs']) || ( isset($_GET['myjobs']) && $_GET['myjobs']!=='true' ) ) ) : ?>

	<form action="<?php echo esc_url( home_url() ); ?>/" method="get" id="searchform">

		<div class="search-wrap">

			<div style="width: 100%;">
                <select id="search" name="s" class="search"style=" background: transparent; border: none;">
					<option value=""  selected='selected'>All Jobs</option>
					<option value="Business Admin">Business Admin</option>
					<option value="Business Analyst">Business Analyst</option>
					<option value="Creative+Digital+Media">Creative & Digital Media</option>
					<option value="Customer Services">Customer Services</option>
					<option value="Digital Marketing">Digital Marketing</option>
					<option value="Financial Services">Financial Services</option>
					<option value="IT Tech">IT</option>
					<option value="Social Media">Social Media</option>
					<option value="Web Development">Web Development</option>
				</select>
				<input type="text" id="near" title="<?php _e('Location',APP_TD); ?>" name="location" class="text" placeholder="<?php _e('Location',APP_TD); ?>" value="<?php if (isset($_GET['location'])) echo esc_attr($_GET['location']); ?>" />
				<label for="search"><button type="submit" title="<?php _e('Go',APP_TD); ?>" class="submit"><?php _e('Go',APP_TD); ?></button></label>

				<input type="hidden" name="ptype" value="<?php echo esc_attr( APP_POST_TYPE ); ?>" />

				<input type="hidden" name="latitude" id="field_latitude" value="" />
				<input type="hidden" name="longitude" id="field_longitude" value="" />
				<input type="hidden" name="full_address" id="field_full_address" value="" />
				<input type="hidden" name="north_east_lng" id="field_north_east_lng" value="" />
				<input type="hidden" name="south_west_lng" id="field_south_west_lng" value="" />
				<input type="hidden" name="north_east_lat" id="field_north_east_lat" value="" />
				<input type="hidden" name="south_west_lat" id="field_south_west_lat" value="" />
			</div>

			<div class="radius">
				<label for="radius">Radius:</label>
				<select name="radius" class="radius">
					<option value="20"  selected='selected'>Auto</option>
					<option value="1" >1 mi</option>
					<option value="5" >5 mi</option>
					<option value="10" >10 mi</option>
					<option value="50" >50 mi</option>
				</select>
			</div><!-- end radius -->

		</div><!-- end search-wrap -->

	</form>

<?php endif; ?>