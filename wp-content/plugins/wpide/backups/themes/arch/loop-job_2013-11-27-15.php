<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "3dd1b34a26c051beee0aad93403697c0ee606c44d4"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/arch/loop-job.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/arch/loop-job_2013-11-27-15.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 * Main loop for displaying jobs
 *
 * @package JobRoller
 * @author AppThemes
 *
 */
?>

<?php appthemes_before_loop( 'job_listing' ); ?>

<?php if (have_posts()) : $alt = 1; ?>

    <ol class="jobs">

        <?php while (have_posts()) : the_post(); ?>
		
			<?php appthemes_before_post( 'job_listing' ); ?>

            <?php
				$post_class = array('job');
				$expired = jr_check_expired( $post );

				if ( $expired ) {
					$post_class[] = 'job-expired';
				}
				$alt=$alt*-1;

				if ($alt==1) $post_class[] = 'job-alt';

				if ( !empty($main_wp_query) && jr_is_listing_featured( $post->ID, $main_wp_query ) ) $post_class[] = 'job-featured';
				
            ?>

            <li class="<?php echo implode(' ', $post_class); ?>">

                <dl>

                    <dt><?php _e('Type',APP_TD); ?></dt>
                    <dd class="type" style="margin-top: 3px;" ><?php the_post_thumbnail('thumb'); ?></dd>

                    <dt><?php _e('Job', APP_TD); ?></dt>
					
					<?php appthemes_before_post_title( 'job_listing' ); ?>

                    <dd class="title">
						<strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
						<?php jr_job_author(); ?>
                    </dd>

					<?php appthemes_after_post_title( 'job_listing' ); ?>

                    <dt><?php _e('Location', APP_TD); ?></dt>
					<dd class="location"><?php jr_location(); ?></dd>

                    <dt><?php _e('Date Posted', APP_TD); ?></dt>
                    <dd class="date"><strong><?php echo date_i18n(__('j M',APP_TD), strtotime($post->post_date)); ?></strong> <span class="year"><?php echo date_i18n(__('Y',APP_TD), strtotime($post->post_date)); ?></span></dd>
                </dl>
              <div style="margin-top: 10px; padding-top: 10px; border-top: 1px solid #dbdbdb;">
                <?php the_excerpt(); ?>
              </div> 
            </li>
			
			<?php appthemes_after_post( 'job_listing' ); ?>

        <?php endwhile; ?>
		
		<?php appthemes_after_endwhile( 'job_listing' ); ?>

    </ol>

<?php else: ?>

	<?php appthemes_loop_else( 'job_listing' ); ?>        
	
<?php endif; ?>

<?php appthemes_after_loop( 'job_listing' ); ?>