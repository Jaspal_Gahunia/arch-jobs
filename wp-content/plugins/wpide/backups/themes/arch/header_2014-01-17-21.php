<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "3dd1b34a26c051beee0aad93403697c0460b893e00"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/arch/header.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/arch/header_2014-01-17-21.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><script type="text/javascript">//<![CDATA[
	// Google Analytics for WordPress by Yoast v4.2.3 | http://yoast.com/wordpress/google-analytics/
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount','UA-40638938-1']);
	_gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	//]]></script>

<div id="header">
	<div class="fixedwidth clearfix">
		<div class="left">
	    	<h1><a class="logo" href="http://www.archapprentices.co.uk">Arch Apprenticeships</a></h1>		
		</div>
		<div class="right">
			<div class="header-cta"></div>
		</div>

        <div class="nav-band clearfix" style="height: 50px;">
        	
        	
        	<ul class="sf-menu clearfix">
			<li class="home menu-item menu-item-type-post_type menu-item-object-page menu-item-187 first"><a href="http://www.archapprentices.co.uk/"  tabindex="1" ><span>Home</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-186"><a href="http://www.archapprentices.co.uk/aboutarch/"  tabindex="2" >About Arch</a><div class="ul"><span class="dd-top"></span><div class="ul-shadow"><div class="ul-items"><ul>	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4379 first"><a href="http://www.archapprentices.co.uk/aboutarch/" >Arch overview</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3771"><a href="http://www.archapprentices.co.uk/aboutarch/about-agilisys/" >About Agilisys</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3772"><a href="http://www.archapprentices.co.uk/aboutarch/agilisys-values/" >Our Values</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3770"><a href="http://www.archapprentices.co.uk/aboutarch/the-arch-board/" >The Arch Board</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2139"><a href="http://www.archapprentices.co.uk/aboutarch/digital-marketing-pilot/" >Digital Marketing Pilot</a></li>
</ul></div></div><span class="dd-bottom"></span></div></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4826"><a href="http://www.archapprentices.co.uk/for-apprentices/overview/"  tabindex="3" >For apprentices</a><div class="ul"><span class="dd-top"></span><div class="ul-shadow"><div class="ul-items"><ul>	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4734 first"><a href="http://www.archapprentices.co.uk/for-apprentices/overview/" >For apprentices &#8211; Overview</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-618"><a href="http://www.archapprentices.co.uk/for-apprentices/benefits-an-arch-apprentice-gets/" >Benefits of being an Apprentice through Agilisys Arch</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2204"><a href="http://www.archapprentices.co.uk/for-apprentices/what-do-we-look-for-in-our-apprentices/" >What do we look for in our Apprentices?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-278"><a href="http://www.archapprentices.co.uk/for-apprentices/what-can-i-study/" >What apprenticeships can I do?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4620"><a href="http://www.archapprentices.co.uk/for-apprentices/what-our-apprentices-have-to-say/" >What Our Apprentices Have To Say</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-185"><a href="http://www.archapprentices.co.uk/for-apprentices/what-are-my-career-prospects/" >What are my career prospects?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-628"><a href="http://www.archapprentices.co.uk/for-apprentices/how-do-i-apply/" >How do I apply?</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-183"><a href="http://www.archapprentices.co.uk/for-apprentices/faq-apprentices/" >FAQ&#8217;s</a></li>
</ul></div></div><span class="dd-bottom"></span></div></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1537"><a href="http://www.archapprentices.co.uk/for-employer/"  tabindex="4" >For employers</a><div class="ul"><span class="dd-top"></span><div class="ul-shadow"><div class="ul-items"><ul>	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4376 first"><a href="http://www.archapprentices.co.uk/for-employer/" >Overview</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4538"><a href="http://www.archapprentices.co.uk/for-employer/government-financial-support/" >Government financial support</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3439"><a href="http://www.archapprentices.co.uk/for-employer/what-employers-of-arch-apprentices-say/" >What employers of Arch Apprentices say</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1538"><a href="http://www.archapprentices.co.uk/for-employer/roles-and-skills/" >Apprenticeships that Arch delivers</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1539"><a href="http://www.archapprentices.co.uk/for-employer/working-for-you/" >How it works</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2617"><a href="http://www.archapprentices.co.uk/for-employer/apprentice-of-the-month/" >Apprentice of the Month</a></li>
</ul></div></div><span class="dd-bottom"></span></div></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-511"><a href="http://www.archapprentices.co.uk/job-overview/"  tabindex="5" >Apprenticeship Vacancies</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2725"><a href="http://www.archapprentices.co.uk/arch-way/"  tabindex="6" >Pre-Apprenticeship</a><div class="ul"><span class="dd-top"></span><div class="ul-shadow"><div class="ul-items"><ul>	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3524 first"><a href="http://www.archapprentices.co.uk/arch-way/" >Arch Way</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3227"><a href="http://www.archapprentices.co.uk/testimonials/" >Testimonials for Arch Way</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3232"><a href="http://www.archapprentices.co.uk/work-experience/" >Work Experience</a></li>
</ul></div></div>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-525"><a href="http://www.archapprentices.co.uk/contact/"  tabindex="8" >Contact Us</a></li>
			
			</ul>
			
			
        </div>
    </div>
</div>