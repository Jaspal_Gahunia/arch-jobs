<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "3dd1b34a26c051beee0aad93403697c076ee43600e"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/arch/loop-job.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/arch/loop-job_2013-12-12-17.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 * Main loop for displaying jobs
 *
 * @package JobRoller
 * @author AppThemes
 *
 */
?>

<?php appthemes_before_loop( 'job_listing' ); ?>

<?php if (have_posts()) : $alt = 1; ?>

    <ol id="job_list" class="jobs">

        <?php while (have_posts()) : the_post(); ?>
		
			<?php appthemes_before_post( 'job_listing' ); ?>

            <?php
				$post_class = array('job');
				$expired = jr_check_expired( $post );

				if ( $expired ) {
					$post_class[] = 'job-expired';
				}
				$alt=$alt*-1;

				if ($alt==1) $post_class[] = 'job-alt';

				if ( !empty($main_wp_query) && jr_is_listing_featured( $post->ID, $main_wp_query ) ) $post_class[] = 'job-featured';
				
            ?>

            <li class="<?php echo implode(' ', $post_class); ?>">

                <dl>

                    <dd class="type" style="float: right; text-align: center;margin-top: 3px;" ><?php the_post_thumbnail('thumb'); ?></dd>

                    <dt><?php _e('Job', APP_TD); ?></dt>
					
					<?php appthemes_before_post_title( 'job_listing' ); ?>

                    <dd class="title" style="width: 80%;">
						<strong><a class="job_title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
						<?php jr_job_author(); ?>
                    </dd>

					<?php appthemes_after_post_title( 'job_listing' ); ?>
					
                </dl>
              <div style="margin-top: 10px; padding-top: 5px;">
                <?php echo get_excerpt(225); ?>
                
              </div> 
            </li>
			
			<?php appthemes_after_post( 'job_listing' ); ?>

        <?php endwhile; ?>
		
		<?php appthemes_after_endwhile( 'job_listing' ); ?>

    </ol>

<?php else: ?>

	<?php appthemes_loop_else( 'job_listing' ); ?>        
	
<?php endif; ?>

<?php appthemes_after_loop( 'job_listing' ); ?>