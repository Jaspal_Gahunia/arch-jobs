<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "3dd1b34a26c051beee0aad93403697c0460b893e00"){
                                        if ( file_put_contents ( "/data/www/jobs/wp-content/themes/arch/single-job_listing.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/data/www/jobs/wp-content/plugins/wpide/backups/themes/arch/single-job_listing_2014-01-17-17.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php get_header('search'); ?>
<?php 

$job_slug_custom = get_post_meta($post->ID, 'job_slug_dd', true);

$job_company = get_post_meta($post->ID, '_Company', true);
$job_slug = urlencode($job_slug_custom ? $job_slug_custom : ($post->post_title . ($job_company ? ' - ' . $job_company : '')));

?>
	<div class="section single">

	<?php do_action( 'appthemes_notices' ); ?>

	<?php appthemes_before_loop(); ?>
		
		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>
			
				<?php appthemes_before_post(); ?>

				<?php appthemes_stats_update($post->ID); //records the page hit ?>
				
				<div class="section_header">

					<?php appthemes_before_post_title(); ?>
				
                    <div style="float: right; width: 150px; text-align: center;"><?php the_post_thumbnail('thumb'); ?></div>
					<h3 style="color: #1a1a1a !important;"><?php the_title(); ?></h3>

					<?php appthemes_after_post_title(); ?>
					
					<div class="clear"></div>

				</div><!-- end section_header -->
				
				
				<div style="float: right" id="apply-job">

					<?php if ($url = get_post_meta($post->ID, 'job_url', true)) : ?>
						<a class="applyforjob" href="http://www.archapprentices.co.uk/application-form/?role_id=<?php echo $post->ID; ?>">Apply For This Job Now</a>
					<?php else :?>
						<a class="applyforjob" href="http://www.archapprentices.co.uk/application-form/?role_id=<?php echo $post->ID; ?>">Apply For This Job Now</a>
					<?php endif; ?>
			
				</div>
				
				
				<div class="section_content">

					<?php do_action('job_main_section', $post); ?>

					<h3><?php _e('Job Description', APP_TD); ?></h3>
				
					<?php appthemes_before_post_content(); ?>
				
					<?php the_content(); ?>

					<?php the_job_listing_fields(); ?>

					<?php the_listing_files(); ?>

					<?php appthemes_after_post_content(); ?>

					<?php if (get_option('jr_enable_listing_banner')=='yes') : ?><div id="listingAd"><?php echo stripslashes(get_option('jr_listing_banner')); ?></div><?php endif; ?>

					<?php if (get_option('jr_submit_how_to_apply_display')=='yes' && get_post_meta($post->ID, '_how_to_apply', true)) { ?>

						<h2><?php _e('How to Apply',APP_TD) ?></h2>
						<?php echo apply_filters('jr_how_to_apply_content', get_post_meta($post->ID, '_how_to_apply', true)); ?>

					<?php } ?>

					<div style="float: right" id="apply-job">

					<?php if ($url = get_post_meta($post->ID, 'job_url', true)) : ?>
						<a class="applyforjob" href="http://www.archapprentices.co.uk/application-form/?role_id=<?php echo $post->ID; ?>">Apply For This Job Now</a>
					<?php else :?>
						<a class="applyforjob" href="http://www.archapprentices.co.uk/application-form/?role_id=<?php echo $post->ID; ?>">Apply For This Job Now</a>
					<?php endif; ?>
			
				</div>
				
				<p class="meta"><em><?php the_taxonomies(); ?></em></p>

					<?php if ( get_option('jr_ad_stats_all') == 'yes' && current_theme_supports( 'app-stats' ) ) { ?><p class="stats"><?php appthemes_stats_counter($post->ID); ?></p> <?php } ?>

					<div class="clear"></div>

				</div><!-- end section_content -->

				
				<?php
				// load up theme-actions.php and display the apply form
				do_action('job_footer');
				?>
				<ul class="section_footer" style="display:none;">

					<?php if ($url = get_post_meta($post->ID, 'job_url', true)) : ?>
						<li class="apply"><a href="http://www.archapprentices.co.uk/application-form/?role_id=<?php echo $post->ID; ?>">Apply For This Job Now</a></li>
					<?php else :?>
						<li class="apply"><a href="http://www.archapprentices.co.uk/application-form/?role_id=<?php echo $post->ID; ?>">Apply For This Job Now</a></li>
					<?php endif; ?>
					
					<li class="print"><a href="javascript:window.print();"><?php _e('Print Job',APP_TD); ?></a></li>
					
					<?php if (get_post_meta($post->ID, '_jr_geo_longitude', true) && get_post_meta($post->ID, '_jr_geo_latitude', true)) : ?><li class="map"><a href="#map" class="toggle_map"><?php _e('View Map',APP_TD); ?></a></li><?php endif; ?>
					
					<?php if(function_exists('selfserv_sexy')) { ?><li class="sexy share"><a href="#share_form" class="share"><?php _e('Share Job',APP_TD); ?></a></li><?php } ?>

					<li class="edit-job"><?php the_job_edit_link(); ?></li>

				</ul>
				
				<?php appthemes_after_post(); ?>

			<?php endwhile; ?>

				<?php appthemes_after_endwhile(); ?>

		<?php else: ?>

			<?php appthemes_loop_else(); ?>

		<?php endif; ?>	

		<?php appthemes_after_loop(); ?>

	</div><!-- end section -->	

	<div class="clear"></div>

</div><!-- end main content -->

<?php if (get_option('jr_show_sidebar')!=='no') get_sidebar('job'); ?>