<?php 


if(function_exists("register_field_group"))
{
	register_field_group(array (
	'id' => 'acf_campaign-codes',
	'title' => 'Campaign Codes',
	'fields' => array (
	array (
	'key' => 'field_53358f9c603ee',
	'label' => 'Default Campaign Code',
	'name' => 'axs_default_campaign_code',
	'type' => 'text',
	'default_value' => '',
	'placeholder' => '',
	'prepend' => '',
	'append' => '',
	'formatting' => 'html',
	'maxlength' => '',
	),
	array (
	'key' => 'field_53359057909e9',
	'label' => 'Map campaign code with keywords in referer\'s url',
	'name' => 'axs_campaign_code_referers',
	'type' => 'textarea',
	'default_value' => '',
	'placeholder' => '',
	'maxlength' => '',
	'rows' => '',
	'formatting' => 'none',
	),
	array (
	'key' => 'field_5335928db9546',
	'label' => 'Google Analitics Account ID',
	'name' => 'axs_campaign_code_ga_key',
	'type' => 'text',
	'default_value' => '',
	'placeholder' => '',
	'prepend' => '',
	'append' => '',
	'formatting' => 'html',
	'maxlength' => '',
	),
	array (
	'key' => 'field_533c1edca8c1c',
	'label' => 'Cookie Domain',
	'name' => 'axs_cookie_domain',
	'type' => 'text',
	'default_value' => '',
	'placeholder' => '',
	'prepend' => '',
	'append' => '',
	'formatting' => 'none',
	'maxlength' => '',
	),
	),
	'location' => array (
	array (
	array (
	'param' => 'options_page',
	'operator' => '==',
	'value' => 'acf-options-campaign-codes',
	'order_no' => 0,
	'group_no' => 0,
	),
	),
	),
	'options' => array (
	'position' => 'normal',
	'layout' => 'no_box',
	'hide_on_screen' => array (
	),
	),
	'menu_order' => 0,
	));
}


