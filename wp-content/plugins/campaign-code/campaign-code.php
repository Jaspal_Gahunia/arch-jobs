<?php
/*
Plugin Name: Campaign Code Detection
Description: Campaign code and phone detection
Author: Andy Lobel
Version: 1.0
*/


define ('AXS_CAMPAIGN_CODE_PLUGIN_PATH', plugin_dir_path(__FILE__));


// only include this line if this plugin is not on the main arch site on the test server
include_once(AXS_CAMPAIGN_CODE_PLUGIN_PATH . 'fields.php');




add_action('init', 'axs_campaign_code_init', 0);
function axs_campaign_code_init() {
	
	
	
	if( function_exists('acf_add_options_sub_page') )
	{
		acf_add_options_sub_page(array(
			'title' => 'Campaign Codes',
			'parent' => 'options-general.php',
			'capability' => 'manage_options'
		));
	}
}




/*
 * Public functions   
 */
function axs_phone($phone = '')
{
	global $axs_campaign_code;	
	return $axs_campaign_code->get_phone($phone);
}

function axs_campaign_code($code = '')
{
	global $axs_campaign_code;
	return $axs_campaign_code->get_campaign_code($code);
}




/*
 * Code detection handling class
 */
class axs_campaign_code
{
	
	protected $campaign_code = ''; //detected campaign code
	protected $campaign_info = '';
	protected $phone = ''; //detected phone
	protected $default_campaign_code = '';
	protected $default_phone = '';

	protected $phone_service_enabled = false;

	
	protected $referers = array();
	protected $campaign_code_phones = array();
	
	public function __construct()
	{
		// do stuff on construct
	}
	
	public function init()
	{		


			$this->default_campaign_code = get_field('axs_campaign_code_default', 'option');

			$this->ga_key = get_field('axs_campaign_code_ga_key', 'option');
			$this->default_phone = get_field('axs_phone_default', 'option');

			
			
			//grab campaign mapping from textarea in admin and put into array
			$referers_lines = explode("\r\n", get_field('axs_campaign_code_referers', 'option'));
			foreach ($referers_lines as $line) { // CAMPAIGN_CODE:keyword;keyword;..
				$arr = explode(':', $line); // 0 -campaign code, 1- keywords
				$this->referers[$arr[0]] = explode(';', $arr[1]);
			}			
			
			//grab campaign:phone mapping from textarea in admin and put into array
			$phone_lines = explode("\r\n", get_field('axs_campaign_code_phones', 'option'));
			foreach ($phone_lines as $line) { // CAMPAIGN_CODE:phone				
				$cp_phone = explode(':', $line);
				$this->campaign_code_phones[$cp_phone[0]] = $cp_phone[1];			
			}
		
	}
	
	
	public function detect()
	{
		$this->detect_code();
		$this->detect_phone();
	}	
	
	protected function detect_code()
	{
		// 1. get campaign_code from url
		if (isset($_GET['cp'])) {
			$this->campaign_code = $_GET['cp'];
		}		
		
		// 2. get campaign code mapped to referer url
		if (!$this->campaign_code) {			
			$this->campaign_code = $this->get_code_from_referer(wp_get_referer());
		}		
		
		// 3. get campaign_code from Cookie
		if (!$this->campaign_code) {
			if (isset($_COOKIE['axs_campaign_code'])) {
				$this->campaign_code = $_COOKIE['axs_campaign_code'];
			}
		}
		
		// 4. default campaign_code
		if (!$this->campaign_code) {
			$this->campaign_code = $this->default_campaign_code;
		}
		
		$this->campaign_code = strtoupper($this->campaign_code);
		
		$campaign_name = substr($this->campaign_code, 3, 3);
		
		$this->campaign_info = array(
				
				'Name' => $campaign_name,
				'Channel' => $campaign_name,
				
		);
		

		
		setcookie('axs_campaign_code', $this->campaign_code, strtotime('+60 days'), '/', get_field('axs_cookie_domain', 'option'));		

	}
	
	protected function detect_phone()
	{	
		// 1. mapping
		if (!empty($this->campaign_code_phones[$this->campaign_code])) {
			$this->phone = $this->campaign_code_phones[$this->campaign_code];
		}
		
		
		// 2. default phone
		if (!$this->phone) {
			$this->phone = $this->default_phone;
		}
		
	}	
	
	protected function get_code_from_referer($referer_url)
	{	
		$campaign_code = '';
		
		foreach ($this->referers as $cp => $keywords) {
			foreach($keywords as $keyword) {
				$keyword = trim($keyword);
				if (!empty($keyword) && stripos($referer_url, $keyword) !== false) {
					$campaign_code = $cp;
					break;
				}
			}			
			if ($campaign_code) break;
		}		
		
		return $campaign_code;		
	}
	


	
	//return detected phone if it is set or one provided
	public function get_phone($phone = '')
	{
		if ($this->phone) {
			$phone = $this->phone;
		}
		
		return $phone;
	}	
	
	//return detected campaign_code if it is set or one provided
	public function get_campaign_code($campaign_code = '')
	{
		if ($this->campaign_code) {
			$campaign_code = $this->campaign_code;
		}
		
		return $campaign_code;
	}
	
	public function ga_events_script() {
	
		if ($this->ga_key): 	

		
			$campaign_name    = isset($this->campaign_info['Name']) ? $this->campaign_info['Name'] : '';
			$campaign_channel = isset($this->campaign_info['Channel']) ? $this->campaign_info['Channel'] : '';
			
			?>
			<script type="text/javascript">		
				var axs_campaign_code    = '<?php echo $this->campaign_code ?>';
				var axs_campaign_name    = '<?php echo $campaign_name ?>';
				var axs_campaign_channel = '<?php echo $campaign_channel ?>';
				
				var _gaq = _gaq || [];
				_gaq.push(['_setAccount', '<?php echo $this->ga_key?>']);
				_gaq.push(['_setDomainName', '<?php echo home_url(); ?>']);
				_gaq.push(['_setCustomVar', 1, 'CampaignCode', axs_campaign_code]);
				_gaq.push(['_setCustomVar', 2, 'CampaignName', axs_campaign_name]);
				_gaq.push(['_setCustomVar', 3, 'CampaignChannel', axs_campaign_channel]);
				_gaq.push(['_trackPageview']);
				
				(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>	
			<?php 
			
		endif;
		
	}

}



$axs_campaign_code = new axs_campaign_code();

/*
 * Actions/Filters
 */
add_action('init', array($axs_campaign_code, 'init'),0);
add_action('init', array($axs_campaign_code, 'detect'));
add_action('wp_head', array($axs_campaign_code, 'ga_events_script'),0);


//campaign code can be retrieved by other plugins through this filter
add_filter('axs_campaign_code', array($axs_campaign_code, 'get_campaign_code'));
add_filter('axs_phone', array($axs_campaign_code, 'get_phone'));



