<?php
/*
Plugin Name: CNT Grandchild theme plugin
Description: Grandchild theme for Jobroller build as a plugin.
Author: rumo
Version: 1.0
*/

class CNT_Jobroller_Grandchild
{
	protected static $instance;

	public static function instance() 
	{
		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof CNT_Jobroller_Grandchild ) ) {
			self::$instance = new CNT_Jobroller_Grandchild;

			register_activation_hook( __FILE__, array(self::$instance , 'install' ) );
			self::$instance->setup_constants();
			self::$instance->includes();
			self::$instance->add_actions();
		}

		return self::$instance;
	}
	
	function setup_constants() 
	{
		define('CNT_JOBROLLER_GRANDCHILD_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
		define('CNT_JOBROLLER_GRANDCHILD_PLUGIN_URL', plugin_dir_url( __FILE__ ));
	}

	function includes() {}
	function install() {}
	
	function add_actions() 
	{
		//add job url field enable Apply online form
		add_action('add_meta_boxes', array($this,'add_meta_boxes') );
		add_action('jr_after_submit_job_form', array($this, 'enable_apply_online_form'));		
		add_action('save_post', array($this, 'save_post'));


		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
		//add_filter('template_include', array($this, 'template_include'));

		//rss feed template
		remove_all_actions( 'do_feed_rss2' );
		add_action( 'do_feed_rss2', array($this, 'rss_template'), 10, 1 );

		//overide some default jobroller stuff
		add_action( 'init', array($this, 'appthemes_setup_orders'), 11 );

		add_filter('jr_email_signature', array($this, 'email_signature'), 10, 3);
		
		remove_filter('sanitize_user', 'strtolower');
	}

	function enqueue_scripts() 
	{
		wp_enqueue_script('jquery');
		wp_enqueue_style('cnt-jobroller-grandchild-style', CNT_JOBROLLER_GRANDCHILD_PLUGIN_URL . '/grandchild-styles.css' );
		wp_enqueue_script('cnt-jobroller-grandchild-script', CNT_JOBROLLER_GRANDCHILD_PLUGIN_URL . '/grandchild-scripts.js' );
	}

	function template_include()
	{
		if (file_exists( untrailingslashit( CNT_JOBROLLER_GRANDCHILD_PLUGIN_DIR . '/templates/' . basename( $template ))))
			$template = untrailingslashit( CNT_JOBROLLER_GRANDCHILD_PLUGIN_DIR . '/templates/' . basename( $template ));

		return $template;		
	}

	function rss_template($for_comments) 
	{
	    $rss_template = CNT_JOBROLLER_GRANDCHILD_PLUGIN_DIR . '/templates/feed-rss2.php';	    
	    load_template( $rss_template );	    
	}
	


	function add_favicon()
	{
		?>
		<link type="image/x-icon" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon.ico?v=1" rel="shortcut icon" />
		<?php 
	}

	function get_city($post)
	{
			$city = get_post_meta($post->ID, 'geo_short_address', true);

			return $city;
	}

	function get_company($post)
	{	
			$company_name = wptexturize(strip_tags(get_post_meta($post->ID, '_Company', true)));

			return $company_name;
	}


	function enable_apply_online_form($job)
	{
		$job_url = get_post_meta($job->ID, 'job_url', true);
		?>
		<fieldset>
			<legend>Apply Online Form</legend>
			<p class="optional">Enter a link/url to an external job webpage for applicants to use to apply for your job, or leave blank to enable applications directly on <?php bloginfo('name'); ?></p>
			<p class="optional">
				<label for="job_url">URL</label>
				<input type="text" class="text" name="job_url" id="job_url" value="<?php echo esc_url($job_url) ?>" />
				<?php wp_nonce_field( 'joburl', 'job_url_nonce') ?>
			</p>
		</fieldset>

		<?php
		return $job;
	}

	function save_post($post_id)
	{	
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      		return $post_id;

		if (!(isset($_POST['action']) && in_array($_POST['action'] , array('edit-job', 'new-job', 'editpost'))))
			return $post_id;

		if (! (isset($_POST['job_url_nonce']) && wp_verify_nonce($_POST['job_url_nonce'], 'joburl')))
			return $post_id;

		$post = get_post($post_id);		

		if ($post->post_type != 'job_listing')
			return $post_id;

		$job_url = isset($_POST['job_url']) ? esc_url($_POST['job_url']) : '';
		update_post_meta($post_id, 'job_url', $job_url);

		return $post_id;
	}	

	function add_meta_boxes()
	{
		add_meta_box(
	        'job_url_meta_box',
	        'Apply Online Form',
	        array($this, 'job_url_meta_box'),
	        'job_listing',
	        'side',
	        'core'
	    );
	}

	function job_url_meta_box($post)
	{
		$job_url = get_post_meta($post->ID, 'job_url', true);
		?>
			<p>
Enter a link/url to an external job webpage for applicants to use to apply for your job, or leave blank to enable applications directly on <?php bloginfo('name'); ?></p>
			<label for="job_url">URL</label>
			<input type="text" class="text" name="job_url" id="job_url" value="<?php echo esc_url($job_url) ?>" />
		<?php wp_nonce_field( 'joburl', 'job_url_nonce') ?>
		<?php
	}


	function appthemes_setup_orders()
	{	
		//Change 'Tax' to UK VAT on payments
		APP_Item_Registry::register( '_regional-tax', __( 'UK VAT', APP_TD ), array(), 99 );
	}


	function email_signature($signature, $identifier, $content_type = 'plain' )
	{
		if ( 'html' == $content_type ) {
			$signature .= "<p>
							Contentive Limited<br>
							1 Hammersmith Broadway<br>
							London<br>
							United Kingdom<br>
							W6 9DL<br>
						</p>
 						<p>
							VAT Number: 159 0315 17
						</p>";
		} else {
			$signature .= PHP_EOL.'1 Hammersmith Broadway'.PHP_EOL.'London'.PHP_EOL.'United Kingdom'.PHP_EOL.'W6 9DL'.PHP_EOL.PHP_EOL.'VAT Number: 159 0315 17';			
		}

		return $signature;
	}

}

function CNTJG()
{
	return CNT_Jobroller_Grandchild::instance();
}
CNTJG();