<?php
/*
Plugin Name: CNT Security
Description: Security patches
Author: rumo
Version: 0.1
*/

/**
 * Login page add autocomple="off" to password field
 */
add_action('login_init', 'cnts_autocomplete_login_init');
function cnts_autocomplete_login_init()
{
    ob_start();
}

add_action('login_form', 'cnts_autocomplete_login_form');
function cnts_autocomplete_login_form()
{
    $content = ob_get_clean();
    $content = str_replace('id="user_pass"', 'id="user_pass" autocomplete="off"', $content);
    $content = str_replace('id="loginform"', 'id="loginform" autocomplete="off"', $content);

    echo $content;
}

/**
 * Escape html for every input input
 * Escape it for frontend only. To determine it I use HTTP_REFERER which is unreliable and can be faked.
 */
add_action( 'muplugins_loaded', 'cnts_check_is_admin' );

function cnts_check_is_admin() {
    $is_admin = strpos($_SERVER['HTTP_REFERER'], 'wp-admin');
    if ($is_admin === false) {
        cnts_escape_inputs(true);
    }
    else {
        //quite late to run but still, in case someone pretends to post from wp-admin
        //add_action('set_current_user', 'cnts_double_check_is_admin');
    }
}

function cnts_double_check_is_admin()
{
    //only logged in user can be in admin panel
    if (!is_user_logged_in()) {
        cnts_escape_inputs(true);
    }
    //even logged in should be allowed to post html (hope those are most common capabilities)
    elseif (!current_user_can( 'unfiltered_html') || !current_user_can('edit_theme_options')) {
       cnts_escape_inputs(true);
    }



}

function cnts_escape_inputs($strip_all_html = false)
{
    foreach ($_POST as $key => $value ) {
        $_POST[$key] = cnts_escape($value, $strip_all_html);
    }

    foreach ($_GET as $key => $value ) {
        $_GET[$key] = cnts_escape($value, $strip_all_html);
    }

    foreach ($_REQUEST as $key => $value ) {
        $_REQUEST[$key] = cnts_escape($value, $strip_all_html);
    }
}

function cnts_escape($data, $strip_all_html = false)
{
    if ($strip_all_html) {
        return wp_kses($data, 'strip');
    }
    else {
        return wp_kses($data, 'post');
    }
}



/*
 * Jobroller specific vulnerabilities
 */

//map validation
if (isset($_POST['jr_geo_latitude'])) {
    $_POST['jr_geo_latitude'] = floatval($_POST['jr_geo_latitude']);
}

if (isset($_POST['jr_geo_longitude'])) {
    $_POST['jr_geo_longitude'] = floatval($_POST['jr_geo_longitude']);
}

//check refferers
/*
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $home = parse_url(home_url());
    $ref  = parse_url($_SERVER['HTTP_REFERER']);

    if ($home['host'] != $ref['host']) {
        error_log('Oops. Something went wrong. (mu-plugins/cntSecurity.php)');
        wp_die('Oops. Something went wrong.');    
    }
}
*/

