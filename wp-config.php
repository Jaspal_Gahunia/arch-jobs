<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// If WordPress is behind reverse proxy
// which proxies https to http
if ( (!empty( $_SERVER['HTTP_X_FORWARDED_HOST'])) ||
     (!empty( $_SERVER['HTTP_X_FORWARDED_FOR'])) ) {

    // http://wordpress.org/support/topic/wordpress-behind-reverse-proxy-1
    $_SERVER['HTTP_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];

    define('WP_HOME', 'https://jobs.archapprentices.co.uk');
    define('WP_SITEURL', 'https://jobs.archapprentices.co.uk');

    // rewrite blog word with wordpress
    //$_SERVER['REQUEST_URI'] = str_replace("wordpress", "blog",
    //$_SERVER['REQUEST_URI']);

    // http://wordpress.org/support/topic/compatibility-with-wordpress-behind-a-reverse-proxy
    $_SERVER['HTTPS'] = 'on';
}

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'arch_jobs_live');

/** MySQL database username */
define('DB_USER', 'arch_jobs_live');

/** MySQL database password */
define('DB_PASSWORD', 'Kdf3JFmm14f');

/** MySQL hostname */
define('DB_HOST', '10.124.3.8');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N^|=aabp55Hc(:4~$TY02$`TC/qq#@@#Xk1lChZ+J=} _>;ys@o7Mr:E1,|[{<f.');
define('SECURE_AUTH_KEY',  'e@]eOO5w{_~09mqko_]sEF<ycW66nm8KSte%wz7(hZjy[W)D8?NZnk$)jz#4P~2]');
define('LOGGED_IN_KEY',    ')E0D},!4cH%b,X0Y9]X-V>b{oOD6M<6xu7*1s2}[gx^i}LiD]mm<0ULd;[};8<9q');
define('NONCE_KEY',        'eT5~n0z6H^k[juRZH/eix(4OXYOXpiD:XH2&TX568m}1k`[MY48=V6Jkdplf59=<');
define('AUTH_SALT',        'r/,|DS)M+(G|?g@_&NEVy]/2o=~7tQiTG=!:b4+ P2+_J_<L}kYDkw0HJ+yWB:PL');
define('SECURE_AUTH_SALT', 'd!-u&]%)O5/m;bZGqIYd;t27|/45z)SD^b[j Dm*13L_A-96_U}C*hLh+wdfWf]p');
define('LOGGED_IN_SALT',   'WmH=#Xf-9)N,[~:Frr?l.0ZM]`s E#9GJX%Q&MG.,8c<OQ:*7cO56=9tGO $dlDi');
define('NONCE_SALT',       'cYL`~mBD3K4ZE$~h(WBT-bLGGv<[L%?g`gD*QK(ve(T`%~<Kh{*nCx{+=|<@dk,z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'jobs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_CACHE', true); // Added by W3 Total Cache

define('MAIN_SITE_URL', 'https://www.archapprentices.co.uk/');
//define('SUNRISE', 'on');

//if (file_exists( dirname(__FILE__) . '/custom-config.php'))
//    include dirname(__FILE__) . '/custom-config.php';

//if ( !defined('DISALLOW_FILE_MODS') )
//    define( 'DISALLOW_FILE_MODS', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

